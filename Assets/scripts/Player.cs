﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public float maxSpeed = 3;
	public float speed = 20f;
	public float jumpPower = 500f;

	public bool grounded;
	public bool canDoubleJump;

	private Rigidbody2D rigidBody2D;
	private Animator animator;

	void Start () {
		rigidBody2D = gameObject.GetComponent<Rigidbody2D>();
		animator = gameObject.GetComponent<Animator>();

	}

	void Update () {
		animator.SetBool("Grounded", grounded);
		animator.SetFloat("speed", Mathf.Abs(rigidBody2D.velocity.x));

		if(Input.GetAxis("Horizontal") < -0.1f){
			transform.localScale = new Vector3(-1, 1, 0f);

		}

		if(Input.GetAxis("Horizontal") > 0.1f){
			transform.localScale = new Vector3(1, 1, 0f);

		}

		if(Input.GetButtonDown("Vertical")){
			if(grounded){
				rigidBody2D.AddForce(Vector2.up * jumpPower);
				canDoubleJump = true;

			}else{
				if(!grounded){
					rigidBody2D.gravityScale = 1;

				}

				if(canDoubleJump){
					canDoubleJump = false;
					rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, 0);
					rigidBody2D.AddForce(Vector3.up * jumpPower);

				}

			}
		}

	}

	void FixedUpdate(){

		Vector3 easeVelocity = rigidBody2D.velocity;
		easeVelocity.y = rigidBody2D.velocity.y;
		easeVelocity.z = 0.0f;
		easeVelocity.x *= 0.75f;

		float h = Input.GetAxis("Horizontal");

		if(grounded){
			rigidBody2D.velocity = easeVelocity;

		}

		//player movement on x-Axis using a/d/left/right
		rigidBody2D.AddForce((Vector2.right * speed) * h);

		//limmiting speed of player 
		if(rigidBody2D.velocity.x > maxSpeed){
			rigidBody2D.velocity = new Vector2 (maxSpeed, rigidBody2D.velocity.y);
		}

		if(rigidBody2D.velocity.x < -maxSpeed){
			rigidBody2D.velocity = new Vector2 (-maxSpeed, rigidBody2D.velocity.y);
		}
	}
}